if [ $# -eq 0 ]
  then
    echo "Please choose container name:"
    docker ps --format '{{.Names}}'
    exit
fi

docker exec -it $1 /bin/bash
