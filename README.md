## Installation
1. [Download](https://www.docker.com/products/docker-desktop) and install Docker desktop
2. Clone project `git clone https://dtikhonenko@bitbucket.org/dtikhonenko/test-project.git`
3. Go to project folder and run `docker-compose up -d`
4. Run `run_migrations.sh` shell script to import db structure and test fixtures
5. Update /etc/hosts file on your host machine 

## Connect to postgres container
- Connect to container by ssh: `run container_bash.sh` without arguments to show all runing containers names^ then run `container_bash.sh container_name`
- Inside db container you can open psql client using this command: ` psql -U root -h psql -p 5432 test_db`
