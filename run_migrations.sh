#!/bin/bash

# Execute all migrations and load test fixtures 

docker exec -it -w "/var/www/api-app/bin" test-app_php_1  php console doctrine:migrations:migrate --no-interaction

docker exec -it -w "/var/www/api-app/bin" test-app_php_1  php console doctrine:fixtures:load --no-interaction
